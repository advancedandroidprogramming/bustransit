package com.example.bustransit.ViewModelTest

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Notification
import com.example.bustransit.Data.Local.Model.Ticket
import com.example.bustransit.Repository.NotificationRepository
import com.example.bustransit.ViewModel.NotificationViewModel
import com.example.bustransit.ViewModel.TicketViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class NotificationViewModelTest {
    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: NotificationViewModel
    val client= Notification(1,"dfsdgsf",1,1)
    val client1= Notification(1,"dfsdgsf",1,1)
    val client2= Notification(1,"dfsdgsf",1,1)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()



    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = AppDatabase.getDatabase(context)
        val companyRepository: NotificationRepository = NotificationRepository(Application())
        // viewModel = CompanyViewModel(plantRepo, company.Id)

        viewModel= NotificationViewModel(Application())
        viewModel.addBus(client)
        viewModel.addBus(client2)

    }


    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testGetAllAppointments(){
        MatcherAssert.assertThat(getValue(viewModel.allBus).size, Matchers.equalTo(3))
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDeleteAppointments() {
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.deleteBus(client1)
            //  MatcherAssert.assertThat(getValue(viewModel.deleteAppointment(companny1))), Matchers.equalTo(2)
        }
    }


    @Test
    @Throws(InterruptedException::class)
    fun testUdateAppointments() {
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.updateBus(client2)
            //  MatcherAssert.assertThat(getValue(viewModel.deleteAppointment(companny1))), Matchers.equalTo(2)
        }
    }


    @Test
    @Throws(InterruptedException::class)
    fun testDefaultData(){
        //  Assert.assertTrue(viewModel.equals(Appointment(1, 1, 2, "", "",4,6)))
    }
}
