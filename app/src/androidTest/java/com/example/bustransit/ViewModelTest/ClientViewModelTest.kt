package com.example.bustransit.ViewModelTest


import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Bus
import com.example.bustransit.Data.Local.Model.Client
import com.example.bustransit.Repository.BusRepository
import com.example.bustransit.Repository.ClientRepository
import com.example.bustransit.Repository.TicketRepository
import com.example.bustransit.ViewModel.BusViewModel
import com.example.bustransit.ViewModel.ClientViewModel
import com.example.bustransit.ViewModel.TicketViewModel
import junit.framework.Assert
import junit.framework.Assert.assertFalse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
class ClientViewModelTest {
    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: ClientViewModel
    val client= Client(1,"sdfsdfs","sdfsdf",1213,"sdfsdf")
    val client1= Client(1,"sdfsdfs","sdfsdf",1213,"sdfsdf")
    val client2= Client(1,"sdfsdfs","sdfsdf",1213,"sdfsdf")

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()



    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = AppDatabase.getDatabase(context)
        val companyRepository: ClientRepository = ClientRepository(Application())
        // viewModel = CompanyViewModel(plantRepo, company.Id)

        viewModel=ClientViewModel(Application())
        viewModel.addClient(client)
        viewModel.addClient(client2)

    }


    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testGetAllAppointments(){
        MatcherAssert.assertThat(getValue(viewModel.allClient).size, Matchers.equalTo(3))
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDeleteAppointments() {
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.deleteClient(client1)
            //  MatcherAssert.assertThat(getValue(viewModel.deleteAppointment(companny1))), Matchers.equalTo(2)
        }
    }


    @Test
    @Throws(InterruptedException::class)
    fun testUdateAppointments() {
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.updateClient(client2)
            //  MatcherAssert.assertThat(getValue(viewModel.deleteAppointment(companny1))), Matchers.equalTo(2)
        }
    }


    @Test
    @Throws(InterruptedException::class)
    fun testDefaultData(){
        //  Assert.assertTrue(viewModel.equals(Appointment(1, 1, 2, "", "",4,6)))
    }
}