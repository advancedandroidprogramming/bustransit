package com.example.bustransit.ViewModelTest

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Ticket
import com.example.bustransit.Repository.TicketRepository
import com.example.bustransit.ViewModel.TicketViewModel

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class TicketViewModelTest {
    private lateinit var appDatabase: AppDatabase
    private lateinit var viewModel: TicketViewModel
    val client= Ticket(1,231,1,1)
    val client1= Ticket(1,231,1,1)
    val client2= Ticket(1,231,1,1)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()



    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = AppDatabase.getDatabase(context)
        val companyRepository: TicketRepository = TicketRepository(Application())
        // viewModel = CompanyViewModel(plantRepo, company.Id)

        viewModel= TicketViewModel(Application())
        viewModel.buyTicket(client)
        viewModel.buyTicket(client2)

    }


    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testGetAllAppointments(){
        MatcherAssert.assertThat(getValue(viewModel.allTicket).size, Matchers.equalTo(3))
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDeleteAppointments() {
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.deleteTicket(client1)
            //  MatcherAssert.assertThat(getValue(viewModel.deleteAppointment(companny1))), Matchers.equalTo(2)
        }
    }


    @Test
    @Throws(InterruptedException::class)
    fun testUdateAppointments() {
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.updateTicket(client2)
            //  MatcherAssert.assertThat(getValue(viewModel.deleteAppointment(companny1))), Matchers.equalTo(2)
        }
    }


    @Test
    @Throws(InterruptedException::class)
    fun testDefaultData(){
        //  Assert.assertTrue(viewModel.equals(Appointment(1, 1, 2, "", "",4,6)))
    }
}
