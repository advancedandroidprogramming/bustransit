package com.example.bustransit.DaoTest

import androidx.room.Room
import androidx.test.filters.SmallTest
import androidx.test.runner.AndroidJUnit4
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Ticket
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class TicketDaoTest {

    private lateinit var database: AppDatabase
    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,"bus_transit").allowMainThreadQueries().build()

    }

    @After
    fun closeDb() = database.close()

    @Test
    fun insertBusAndGet()=runBlockingTest{
        val client= Ticket(1,231,1,1)
        database.notificationDao().addNotification(client)

        val loaded=database.ticketDao().getAllTicket()

        MatcherAssert.assertThat<Ticket>(loaded as Ticket, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.'is'(client.id))
        MatcherAssert.assertThat(loaded.number, CoreMatchers.'is'(client.number))
    }
    @Test
    fun deleteBusAndGet() = runBlockingTest {
        // Given a group inserted
        val client= Ticket(1,231,1,1)
        database.ticketDao().deleteTicket(client)

        // THEN - The object returned is null
        val getCompany = database.ticketDao().findTicketById(client.id)
        MatcherAssert.assertThat(getCompany, CoreMatchers.nullValue())
    }

    @Test
    fun getAllBus()= runBlockingTest{
        val client= Ticket(1,"",1,1)

        val loaded=database.ticketDao().findTicketById(client.id)
        MatcherAssert.assertThat(loaded, CoreMatchers.notNullValue())
    }

}