package com.example.bustransit.DaoTest

import androidx.room.Room
import androidx.test.filters.SmallTest
import androidx.test.runner.AndroidJUnit4
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Notification
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class NotificationDaoTest {

    private lateinit var database: AppDatabase
    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,"bus_transit").allowMainThreadQueries().build()

    }

    @After
    fun closeDb() = database.close()

    @Test
    fun insertBusAndGet()=runBlockingTest{
        val client= Notification(1,"dfsdgsf",1,1)
        database.notificationDao().addNotification(client)

        val loaded=database.clientDao().getAllClient()

        MatcherAssert.assertThat<Notification>(loaded as Notification, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.'is'(client.id))
        MatcherAssert.assertThat(loaded.message, CoreMatchers.'is'(client.message))
    }
    @Test
    fun deleteBusAndGet() = runBlockingTest {
        // Given a group inserted
        val client= Notification(1,"",1,1)
        database.notificationDao().deleteNotification(client)

        // THEN - The object returned is null
        val getCompany = database.notificationDao().findNotificationById(client.id)
        MatcherAssert.assertThat(getCompany, CoreMatchers.nullValue())
    }

    @Test
    fun getAllBus()= runBlockingTest{
        val client= Notification(1,"",1,1)

        val loaded=database.notificationDao().findNotificationById(client.id)
        MatcherAssert.assertThat(loaded, CoreMatchers.notNullValue())
    }

}
