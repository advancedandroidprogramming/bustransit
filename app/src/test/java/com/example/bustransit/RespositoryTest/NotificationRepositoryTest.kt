package com.example.bustransit.RespositoryTest

import androidx.test.filters.MediumTest
import androidx.test.runner.AndroidJUnit4
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest
class NotificationRepositoryTest {
private lateinit var repo: TicketRepository

    private lateinit var database: AppDatabase

    // Executes each user synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()
        repo = TicketRepository(this)

    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val client= Notification(1,"message",1,1)

        // WHEN  - company retrieved by id
        val result  = repo.notificationList

    }

    @Test
    fun getALLAndReload() = runBlockingTest {

        // Given a new company in the persistent repository and a mocked callback
        val client= Notification(1,"message",1,1)
        val client= Notification(1,"message",1,1)


        // When groups are deleted
        repo.findNotificationById(client.id!!.toInt())

        // Then the reload companies list
        val result = repo.allNotification

        // Then the getALL companies list
        val result2 = repo.allNotification

        Assert.assertThat(result, CoreMatchers.nullValue())
        Assert.assertThat(result2, CoreMatchers.nullValue())

    }

}