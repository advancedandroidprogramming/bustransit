package com.example.bustransit.RespositoryTest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.filters.MediumTest
import androidx.test.runner.AndroidJUnit4
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Bus
import com.example.bustransit.Data.Local.Model.Ticket
import com.example.bustransit.Data.Remote.ApiService.BusApiService
import com.example.bustransit.Repository.BusRepository
import com.example.bustransit.Repository.TicketRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest
class ClientRepositoryTest {

    private lateinit var repo: TicketRepository

    private lateinit var database: AppDatabase

    // Executes each user synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()
        repo = TicketRepository(this)

    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val client= Ticket(1,231,1,1)

        // WHEN  - company retrieved by id
        val result  = repo.ticketList

    }

    @Test
    fun getALLAndReload() = runBlockingTest {

        // Given a new company in the persistent repository and a mocked callback
        val client1= Ticket(1,231,1,1)
        val client=2 Ticket(1,231,1,1)


        // When groups are deleted
        repo.findBusById(bus.id!!.toInt())

        // Then the reload companies list
        val result = repo.allBus

        // Then the getALL companies list
        val result2 = repo.allBus

        Assert.assertThat(result, CoreMatchers.nullValue())
        Assert.assertThat(result2, CoreMatchers.nullValue())

    }

}