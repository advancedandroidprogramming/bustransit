package com.example.bustransit.RespositoryTest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.MediumTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith
import androidx.test.runner.AndroidJUnit4
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Bus
import com.example.bustransit.Data.Remote.ApiService.BusApiService
import com.example.bustransit.Repository.BusRepository

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@MediumTest
class BusRepositoryTest {
    private lateinit var repo: BusRepository
    private lateinit var database: AppDatabase

    // Executes each user synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,
            "uniguide_db").allowMainThreadQueries().build()
        repo = BusRepository(database.busDao(), BusApiService.getInstance())

    }

    @After
    fun cleanUp() {
        database.close()
    }
    @Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val bus= Bus(1,"","","","","","","","")


        // WHEN  - company retrieved by id
        val result  = repo.allBus

    }

    @Test
    fun getALLAndReload() = runBlockingTest {

        // Given a new company in the persistent repository and a mocked callback
        val bus=Bus(1,"","","","","","","","")
        val bus1=Bus(1,"","","","","","","","")



        // When groups are deleted
        repo.findBusById(bus.id!!.toInt())

        // Then the reload companies list
        val result = repo.allBus

        // Then the getALL companies list
        val result2 = repo.allBus

        Assert.assertThat(result, CoreMatchers.nullValue())
        Assert.assertThat(result2, CoreMatchers.nullValue())

    }

}