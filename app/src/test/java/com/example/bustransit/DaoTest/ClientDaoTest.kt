package com.example.bustransit.DaoTest

import androidx.room.Room
import androidx.test.filters.SmallTest
import androidx.test.runner.AndroidJUnit4
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Bus
import com.example.bustransit.Data.Local.Model.Client
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class ClientDaoTest {

    private lateinit var database: AppDatabase
    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,"bus_transit").allowMainThreadQueries().build()

    }

    @After
    fun closeDb() = database.close()

    @Test
    fun insertBusAndGet()=runBlockingTest{
        val client= Client(1,"","",1213,"")
        database.clientDao().addClient(client)

        val loaded=database.clientDao().getAllClient()

        MatcherAssert.assertThat<Client>(loaded as Client, CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id, CoreMatchers.'is'(client.id))
        MatcherAssert.assertThat(loaded.username, CoreMatchers.'is'(client.username))
        MatcherAssert.assertThat(loaded.password, CoreMatchers.'is'(client.password))
    }
    @Test
    fun deleteBusAndGet() = runBlockingTest {
        // Given a group inserted
        val client= Client(1,"","",1213,"")
        database.clientDao().deleteClient(client)

        // THEN - The object returned is null
        val getCompany = database.clientDao().findClientById(client.id)
        MatcherAssert.assertThat(getCompany, CoreMatchers.nullValue())
    }

    @Test
    fun getAllBus()= runBlockingTest{
        val client= Client(1,"","",1213,"")

        val loaded=database.clientDao().findClientById(client.id)
        MatcherAssert.assertThat(loaded, CoreMatchers.notNullValue())
    }

}