package com.example.bustransit.DaoTest


import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.SmallTest
import androidx.test.runner.AndroidJUnit4
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Bus
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
//import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@SmallTest
class BusDaoTest {

    private lateinit var database:AppDatabase
    @Before
    fun initDb() {
        // using an in-memory database because the information stored here disappears when the
        // process is killed
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java,"bus_transit").allowMainThreadQueries().build()

    }

    @After
    fun closeDb() = database.close()

    @Test
    fun insertBusAndGet()=runBlockingTest{
        val bus=Bus(1,"","","","","","","","")
        database.busDao().addBus(bus)

        val loaded=database.busDao().getAllBus()

        MatcherAssert.assertThat<Bus>(loaded as Bus,CoreMatchers.notNullValue())
        MatcherAssert.assertThat(loaded.id,CoreMatchers.'is'(bus.id))
        MatcherAssert.assertThat(loaded.route,CoreMatchers.'is'(bus.route))
        MatcherAssert.assertThat(loaded.price,CoreMatchers.'is'(bus.price))
    }
   @Test
    fun deleteBusAndGet() = runBlockingTest {
        // Given a group inserted
        val bus=Bus(1,"","","","","","","","")

        // When deleting a user by id
        database.busDao().deleteBus(bus)
        // THEN - The object returned is null
        val getCompany = database.busDao().findBusById(bus.id)
        MatcherAssert.assertThat(getCompany, CoreMatchers.nullValue())
    }

    @Test
    fun getAllBus()= runBlockingTest{
        val bus=Bus(1,"","","","","","","","")

        val result=database.busDao().findBusById(bus.id)
        MatcherAssert.assertThat(result,CoreMatchers.notNullValue())
    }

}