package com.example.bustransit.Repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Dao.TicketDao
import com.example.bustransit.Data.Local.Model.Ticket
import com.example.bustransit.Data.Remote.ApiService.TicketApiService
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TicketRepository(app:Application)
{
    private val ticketDao: TicketDao
    val ticketList:LiveData<List<Ticket>>

    init {
        val database=AppDatabase.getDatabase(app)
        ticketDao= database.ticketDao()
        ticketList= ticketDao.getAllTicket()
    }

    suspend fun buyTicket(ticket:Ticket)
    {
        withContext(IO) {
            val remoteTicket = com.example.bustransit.Data.Remote.Model.Ticket()
            val response = TicketApiService.getInstance().addTicket(remoteTicket).execute()
            if (response.isSuccessful) {
                val responseId: Int? = response.body()?.id
                if (responseId != null) {
                    ticket.id = responseId
                    ticketDao.buyTicket(ticket)
                }
            }
        }

    }

    suspend fun deleteTicket(ticket: Ticket)
    {
        withContext(IO){
            ticketDao.deleteTicket(ticket)
            if(ticket.id != null)
            {
                val response=TicketApiService.getInstance().deleteTicket(ticket.id!!).execute()
                if(!response.isSuccessful)
                {
	      asyncTicketData()
                }
            }
        }
    }

    suspend fun updateTicket(ticket: Ticket) {
        withContext(IO){
            val remoteTicket =com.example.bustransit.Data.Remote.Model.Ticket()
            val response = TicketApiService.getInstance().updateTicket(remoteTicket.id, remoteTicket).execute()
            if (response.isSuccessful) {
                val responseId: Int? = response.body()?.id
                if (responseId != null) {
                    ticket.id = responseId
                    ticketDao.updateTicket(ticket)
                }
            }
        }
    }

    fun asyncTicketData()
    {
        TicketApiService.getInstance().ticketList().enqueue(object : Callback<List<com.example.bustransit.Data.Remote.Model.Ticket>>
        {
            override fun onFailure(call: Call<List<com.example.bustransit.Data.Remote.Model.Ticket>>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<List<com.example.bustransit.Data.Remote.Model.Ticket>>, response: Response<List<com.example.bustransit.Data.Remote.Model.Ticket>>
            ) {
                doAsync {
                    if(response.isSuccessful)
                    {
                        response.body()?.forEach { ticket ->
                            if(ticket.id != null)
                            {
                                val updatedTicket = ticketDao.findTicketById(ticket.id!!)
                                if(updatedTicket == null)
                                {
                                    ticketDao.buyTicket(Ticket(
                                        ticket.id,
                                        ticket.number,1,1
                                    ))
                                }
                                else
                                {
                                    ticketDao.updateTicket(Ticket(
                                        ticket.id,
                                        ticket.number,1,1
                                    ))
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }

        })
    }

    suspend fun findTicketById(id:Int):LiveData<Ticket?>
    {
        return ticketDao.findTicketById(id)
    }
}