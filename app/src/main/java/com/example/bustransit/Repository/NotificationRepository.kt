package com.example.bustransit.Repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Dao.NotificationDao
import com.example.bustransit.Data.Local.Model.Notification
import com.example.bustransit.Data.Remote.ApiService.NotificationApiService
import com.example.bustransit.Data.Remote.Model.Ticket
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationRepository(app:Application) {
    private val notificationDao: NotificationDao
    val ticketList: LiveData<List<Notification>>

    init {
        val database= AppDatabase.getDatabase(app)
        notificationDao= database.notificationDao()
        ticketList= notificationDao.getAllNotification()
    }

    suspend fun sendNotification(notification:Notification)
    {
        withContext(IO) {
            val remoteTicket = com.example.bustransit.Data.Remote.Model.Notification()
            val response = NotificationApiService.getInstance().addNotification(remoteTicket).execute()
            if (response.isSuccessful) {
                val responseId: Int? = response.body()?.id
                if (responseId != null) {
                    notification.id = responseId
                    notificationDao.addNotification(notification)
                }
            }
        }

    }

    suspend fun deleteTicket(ticket: Notification)
    {
        withContext(IO){
            notificationDao.deleteNotification(ticket)
            if(ticket.id != null)
            {
                val response=NotificationApiService.getInstance().deleteNotification(ticket.id!!).execute()
                if(!response.isSuccessful)
                {
                    asyncNotificationData()
                }
            }
        }
    }

    suspend fun updateTicket(ticket: Notification) {
        withContext(IO){
            val remoteTicket =com.example.bustransit.Data.Remote.Model.Notification()
            val response = NotificationApiService.getInstance().updateNotification(remoteTicket.id, remoteTicket).execute()
            if (response.isSuccessful) {
                val responseId: Int? = response.body()?.id
                if (responseId != null) {
                    ticket.id = responseId
                    notificationDao.updateNotification(ticket)
                }
            }
        }
    }

    fun asyncNotificationData()
    {
        NotificationApiService.getInstance().notificationList().enqueue(object : Callback<List<com.example.bustransit.Data.Remote.Model.Notification>>
        {
            override fun onFailure(call: Call<List<com.example.bustransit.Data.Remote.Model.Notification>>, t: Throwable
            ) {

            }

            override fun onResponse(call: Call<List<com.example.bustransit.Data.Remote.Model.Notification>>, response: Response<List<com.example.bustransit.Data.Remote.Model.Notification>>
            ) {
                doAsync {
                    if(response.isSuccessful)
                    {
                        response.body()?.forEach { ticket ->
                            if(ticket.id != null)
                            {
                                val updatedTicket = notificationDao.findNotificationById(ticket.id!!)
                                if(updatedTicket == null)
                                {
                                    notificationDao.addNotification(Notification(
                                        ticket.id,ticket.message,1,1
                                    ))
                                }
                                else
                                {
                                    notificationDao.updateNotification(Notification(
                                        ticket.id,
                                        ticket.message,1,1
                                    ))
                                }
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }

        })
    }

    fun findTicketById(id:Int):LiveData<Notification?>
    {
        return notificationDao.findNotificationById(id)
    }
}