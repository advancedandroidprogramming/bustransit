package com.example.bustransit.Repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Dao.ClientDao
import com.example.bustransit.Data.Local.Model.Client
import com.example.bustransit.Data.Remote.ApiService.ClientApiService
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClientRepository(app: Application) {

    private val clientDao: ClientDao
    val allClient: LiveData<List<Client>>

    init {
        val databse = AppDatabase.getDatabase(app)
        clientDao = databse.clientDao()
        allClient = clientDao.getAllClient()
    }

    suspend fun addClient(client: Client) {
        withContext(IO){
            val remoteClinet = com.example.bustransit.Data.Remote.Model.Client()
            val response = ClientApiService.getInstance().addClient(remoteClinet).execute()
            if (response.isSuccessful) {
                val responseId: Int? = response.body()?.id
                if (responseId != null) {
                    client.id = responseId
                    clientDao.addClient(client)
                }
            }

        }
    }

    suspend fun updateClient(client: Client) {
        withContext(IO){
            val remoteClient = com.example.bustransit.Data.Remote.Model.Client()
            val response = ClientApiService.getInstance().updateClient(remoteClient.id, remoteClient).execute()
            if (response.isSuccessful) {
                val responseId: Int? = response.body()?.id
                if (responseId != null) {
                    client.id = responseId
                    clientDao.updateClient(client)
                }
            }
        }
    }

    suspend fun deleteClient(client: Client) {
        withContext(IO){
            clientDao.deleteClient(client)
            if (client.id != null) {
                val response = ClientApiService.getInstance().deleteClient(client.id!!).execute()
                if (!response.isSuccessful) {
	         asyncClientData()
                }
            }
        }
    }

    fun findClientById(id: Int): LiveData<Client?> {
        return clientDao.findClientById(id)
}

    fun asyncClientData()
    {
        ClientApiService.getInstance().clientList().enqueue(object : Callback<List<com.example.bustransit.Data.Remote.Model.Client>>
        {
            override fun onFailure(call: Call<List<com.example.bustransit.Data.Remote.Model.Client>>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onResponse(call: Call<List<com.example.bustransit.Data.Remote.Model.Client>>, response: Response<List<com.example.bustransit.Data.Remote.Model.Client>>
            ) {
                doAsync {
                    if(response.isSuccessful)
                    {
                        response.body()?.forEach { client ->
                            if(client.id !=  null)
                            {
                                val updatedClient = clientDao.findClientById(client.id!!)
                                if(updatedClient == null)
                                {
                                    clientDao.addClient(Client(
                                        client.id,
                                        client.username,
                                        client.password,
                                        client.phoneNumber,
                                        client.relative
                                    ))
                                }
                                else
                                {
                                    clientDao.updateClient(Client(
                                        client.id,
                                        client.username,
                                        client.password,
                                        client.phoneNumber,
                                        client.relative
                                    ))
                                }
                            }
                            else
                            {

                            }
                        }
                    }
                }
            }

        })
    }

}