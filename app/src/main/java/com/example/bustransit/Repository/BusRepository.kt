package com.example.bustransit.Repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Dao.BusDao
import com.example.bustransit.Data.Local.Model.Bus
import com.example.bustransit.Data.Remote.ApiService.BusApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.doAsync
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BusRepository(app:Application) {

    private val busDao: BusDao
    val allBus: LiveData<List<Bus>>

    init
    {
        val databse = AppDatabase.getDatabase(app)
        busDao=databse.busDao()
        allBus=busDao.getAllBus()

    }

    suspend fun addBusToRoom(bus:Bus)
    {
        withContext(IO){
            busDao.addBus(bus)
        }
    }

    suspend fun addBusToApi(bus:Bus)
    {
        withContext(IO){
            val remoteBus = com.example.bustransit.Data.Remote.Model.Bus()
            val response = BusApiService.getInstance().addBus(remoteBus).execute()
            if(response.isSuccessful)
            {
                val responseId=response.body()?.id
                if(responseId != null)
                {
                    bus.id =responseId
                    busDao.addBus(bus)
                }
            }
        }

    }

    suspend fun updateBusApi(bus:Bus)
    {
        withContext(IO){
            val remoteBus = com.example.bustransit.Data.Remote.Model.Bus()
            val response = BusApiService.getInstance().updateBus(remoteBus.id,remoteBus).execute()
            if(response.isSuccessful)
            {
                val responseId:Int? = response.body()?.id
                if(responseId != null)
                {
                    bus.id = responseId
                    busDao.updateBus(bus)
                }
            }
        }
    }

    suspend fun updateBusRoom(bus:Bus)
    {
        withContext(IO){
            busDao.updateBus(bus)
        }
    }

    suspend fun deleteBusFromRemote(bus: Bus)
    {
        withContext(IO){
            busDao.deleteBus(bus)
            if(bus.id != null)
            {
                val response = BusApiService.getInstance().deleteBus(bus.id!!).execute()
                if(!response.isSuccessful)
                {

                }
            }
        }
    }

    suspend fun deleteBusRoom(bus:Bus)
    {
        withContext(IO){
            busDao.deleteBus(bus)
        }
    }

    suspend fun deleteAllBusRoom()
    {
        withContext(IO){
            busDao.deleteAllBus()
        }
    }

    fun asyncBusData()
    {
        BusApiService.getInstance().busList().enqueue(object  : Callback<List<com.example.bustransit.Data.Remote.Model.Bus>>
        {
            override fun onResponse(call: Call<List<com.example.bustransit.Data.Remote.Model.Bus>>, response: Response<List<com.example.bustransit.Data.Remote.Model.Bus>>
            ) {
                doAsync {
                    if(response.isSuccessful)
                    {
                        response.body()?.forEach { bus ->

                            if(bus.id != null) {
                                val updatedBus = busDao.findBusById(bus.id!!)
                                if(updatedBus == null)
                                {
                                    busDao.addBus(Bus(
                                        bus.id,
                                        bus.number,
                                        bus.price,
                                        bus.route,
                                        bus.startPlace,
                                        bus.destination
                                    ))
                                }
                                else
                                {
                                    busDao.updateBus(Bus(
                                        bus.id,
                                        bus.number,
                                        bus.price,
                                        bus.route,
                                        bus.startPlace,
                                        bus.destination
                                    ))
                                }
                            }
                            else
                            {

                            }

                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<com.example.bustransit.Data.Remote.Model.Bus>>, t: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })

    }

    fun findBusById(id:Int):LiveData<Bus?>
    {
        return busDao.findBusById(id)
    }

}