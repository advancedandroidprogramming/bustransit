package com.example.bustransit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bustransit.R
import com.example.bustransit.data.local.model.Notification
import com.example.bustransit.databinding.NotificationListBinding

class NotificationAdapter(private var notifications:List<Notification>): RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val infalter= LayoutInflater.from(parent.context)
        val binding=infalter.inflate(R.layout.notification_list,parent,false)
        binding.setOnClickListener {
            it.findNavController().navigate(R.id.notification_detail)
        }
        return NotificationViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val binding=(holder as NotificationViewHolder).binding
        binding?.item=notifications[position]
        binding?.executePendingBindings()
    }


    class NotificationViewHolder(view:View):RecyclerView.ViewHolder(view)
    {
        val binding:NotificationListBinding?=DataBindingUtil.bind(view)
    }
}