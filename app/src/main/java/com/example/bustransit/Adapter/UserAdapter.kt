package com.example.bustransit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bustransit.R
import com.example.bustransit.data.local.model.Bus
import com.example.bustransit.data.local.model.User
import com.example.bustransit.databinding.UserListBinding
import kotlinx.android.synthetic.main.bus_list.view.*

class UserAdapter:RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    private var users:List<User> = emptyList()
    fun setUser(users:List<User>)
    {
        this.users=users
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val infalter= LayoutInflater.from(parent.context)
        val binding=infalter.inflate(R.layout.user_list,parent,false)
        /*binding.setOnClickListener {

            it.findNavController().navigate(R.id.detail_bus_nav)
        }*/
        return UserViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        /*val binding=(holder as UserViewHolder).binding
        binding?.item=users[position]
        binding?.executePendingBindings()*/
        users[position].let {
            user -> with(holder){
            itemView.tag=user
            bind(user)
        }
        }
    }

    class UserViewHolder(view: View):RecyclerView.ViewHolder(view)
    {
        val binding:UserListBinding?=DataBindingUtil.bind(view)

        fun bind(user: User)
        {
            binding?.item=user
            binding?.executePendingBindings()
        }
    }
}