package com.example.bustransit.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.bustransit.Data.Local.Model.Client
import com.example.bustransit.R
import kotlinx.android.synthetic.main.fragment_client.*

class ClientAdapter(private val context:Context,private var clientList:List<Client>):RecyclerView.Adapter<ClientAdapter.ClientViewHolder>() {

    fun viewBus(clientList: List<Client>)
    {
        this.clientList=clientList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientViewHolder {
        val inflater=LayoutInflater.from(parent.context)
        val clientRecyclerView:ViewDataBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_client,parent,false)
        return ClientViewHolder(clientRecyclerView)
    }

    override fun getItemCount(): Int {
        return clientList.size
    }

    override fun onBindViewHolder(holder: ClientViewHolder, position: Int) {
        holder.binding(clientList[position])
    }

    class ClientViewHolder(val bind : ViewDataBinding):RecyclerView.ViewHolder(bind.root)
    {
        fun binding(data: Any)
        {
            bind.apply {
                //bus = data
                executePendingBindings()
            }
        }
    }
}