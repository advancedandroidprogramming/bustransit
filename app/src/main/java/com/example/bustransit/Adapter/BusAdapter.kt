package com.example.bustransit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bustransit.R
import com.example.bustransit.data.local.model.Bus
import com.example.bustransit.databinding.BusListBinding
import com.example.bustransit.view.BusFragmentDirections
import com.example.bustransit.viewmodel.BusViewModel
import kotlinx.android.synthetic.main.bus_list.view.*
import kotlinx.android.synthetic.main.fragment_add_bus.view.*
import kotlinx.android.synthetic.main.fragment_add_bus.view.route_text

class BusAdapter():RecyclerView.Adapter<BusAdapter.BusViewHolder>(){

    private var busesList:List<Bus> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusViewHolder {
        val inflater=LayoutInflater.from(parent.context)
        val binding=inflater.inflate(R.layout.bus_list,parent,false)
        binding.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.bus_detail)
        }
        return BusViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return busesList.size
    }

    override fun onBindViewHolder(holder: BusViewHolder, position: Int) {
        busesList[position].let {
            bus -> with(holder) {
                itemView.tag=bus
                bind(bus)
            }
        }
    }

    internal fun setBuses(buses:List<Bus>)
    {
        busesList=buses
        notifyDataSetChanged()
    }

    class BusViewHolder(view:View):RecyclerView.ViewHolder(view)
    {
        val binding:BusListBinding?=DataBindingUtil.bind(view)

        fun bind(bus:Bus)
        {
            binding?.item=bus

        }
    }



    /*private var busesList:List<Bus> = listOf(
        Bus(1,"asfsdf","213","","","","asfsdfsdf","",""),
        Bus(3,"asfsdf","234","","","","asfsdfsf","",""),
        Bus(4,"asfsdf","234","","","","sdfsdfsdf","","")
    )*/

    /*override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusViewHolder {

        val inflater=LayoutInflater.from(parent.context)
        val binding=BusListBinding.inflate(inflater)
        return BusViewHolder(binding)
    }

    override fun getItemCount(): Int = busesList.size


    override fun onBindViewHolder(holder: BusViewHolder, position: Int) {

        busesList[position].let {
            buses -> with(holder){
            itemView.tag=buses
            bind(buses)
        }
        }

    }

    internal fun setBuses(buses:List<Bus>)
    {
        busesList=buses
        notifyDataSetChanged()
    }

    class BusViewHolder(val binding: BusListBinding):RecyclerView.ViewHolder(binding.root)
    {

        fun bind(bus:Bus)
        {
            binding.item=bus
            binding.executePendingBindings()
        }
    }*/


}