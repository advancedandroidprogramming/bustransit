package com.example.bustransit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bustransit.R
import com.example.bustransit.data.local.model.Ticket
import com.example.bustransit.databinding.TicketListBinding

class TicketAdapter(private val tickets:List<Ticket>): RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder {
        val infalter= LayoutInflater.from(parent.context)
        val binding=infalter.inflate(R.layout.ticket_list,parent,false)
        binding.setOnClickListener {
            it.findNavController().navigate(R.id.ticket_detail)
        }
        return TicketViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return tickets.size
    }

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        val binding=(holder as TicketViewHolder).binding
        binding?.item=tickets[position]
        binding?.executePendingBindings()
    }

    class TicketViewHolder(view: View):RecyclerView.ViewHolder(view)
    {
        val binding:TicketListBinding?=DataBindingUtil.bind(view)
    }
}