package com.example.bustransit.Data.Local.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "notification", foreignKeys = arrayOf(
    ForeignKey(entity= Bus::class,parentColumns = arrayOf("id"),childColumns = arrayOf("bus_id"),onDelete= ForeignKey.CASCADE),
    ForeignKey(entity = Client::class, parentColumns = arrayOf("id"), childColumns = arrayOf("client_id"),onDelete= ForeignKey.CASCADE)
))
data class Notification(
    @PrimaryKey @ColumnInfo(name = "id") var id:Int?,
    @ColumnInfo(name = "message") val message:String?,
    val bus_id:Int?,
    val client_id:Int?): Serializable {
}