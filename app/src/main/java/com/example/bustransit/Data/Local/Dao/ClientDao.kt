package com.example.bustransit.Data.Local.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.bustransit.Data.Local.Model.Client

interface ClientDao {
    @Query("select * from client order by username")
    fun getAllClient(): LiveData<List<Client>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addClient(client: Client):Long

    @Query("delete from client")
    fun deleteAllClient()

    @Query("select * from client where id = :clientId")
    fun findClientById(clientId:Int) :LiveData<Client?>

    @Delete
    fun deleteClient(client: Client):Long

    @Update
    fun updateClient(client: Client):Long
}