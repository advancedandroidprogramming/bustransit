package com.example.bustransit.Data.Remote.ApiService

import com.example.bustransit.Data.Remote.Model.Client
import com.example.bustransit.Data.Remote.Model.Ticket
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface TicketApiService {

    @GET("all")
    fun ticketList(): Call<List<Ticket>>

    @POST("add")
    fun addTicket(@Body ticket: Ticket): Call<Ticket>

    @PUT("update/{id}")
    fun updateTicket(@Path("id") id:Int?, @Body ticket: Ticket): Call<Ticket>

    @DELETE("delete/{id}")
    fun deleteTicket(@Path("id") id:Int): Call<ResponseBody>


    companion object {

        private val baseUrl = "http://10.0.2.2:9090/ticket/"

        fun getInstance(): TicketApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(TicketApiService::class.java)
        }
    }
}