package com.example.bustransit.Data.Remote.Model

class Notification {

    var id:Int? = null

    val message:String? = null

    val bus_id:Int?= null

    val client_id:Int?= null
}