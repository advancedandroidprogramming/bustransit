package com.example.bustransit.Data.Remote.ApiService

import com.example.bustransit.Data.Remote.Model.Notification
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface NotificationApiService {

    @GET("all")
    fun notificationList(): Call<List<Notification>>

    @POST("add")
    fun addNotification(@Body notification: Notification): Call<Notification>

    @PUT("update/{id}")
    fun updateNotification(@Path("id") id:Int?, @Body notification: Notification): Call<Notification>

    @DELETE("delete/{id}")
    fun deleteNotification(@Path("id") id:Int): Call<ResponseBody>


    companion object {

        private val baseUrl = "http://10.0.2.2:9090/notification/"

        fun getInstance(): NotificationApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(NotificationApiService::class.java)
        }
    }
}