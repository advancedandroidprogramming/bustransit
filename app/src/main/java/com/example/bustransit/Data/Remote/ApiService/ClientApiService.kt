package com.example.bustransit.Data.Remote.ApiService

import com.example.bustransit.Data.Remote.Model.Bus
import com.example.bustransit.Data.Remote.Model.Client
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ClientApiService {

    @GET("all")
    fun clientList(): Call<List<Client>>

    @POST("add")
    fun addClient(@Body client: Client): Call<Client>

    @PUT("update/{id}")
    fun updateClient(@Path("id") id:Int?, @Body client: Client): Call<Client>

    @DELETE("delete/{id}")
    fun deleteClient(@Path("id") id:Int): Call<ResponseBody>


    companion object {

        private val baseUrl = "http://10.0.2.2:9090/client/"

        fun getInstance(): ClientApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ClientApiService::class.java)
        }
    }
}