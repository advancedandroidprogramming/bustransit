package com.example.bustransit.Data.Local.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import java.io.Serializable
@Entity(tableName = "ticket", foreignKeys = arrayOf(
        ForeignKey(entity= Bus::class,parentColumns = arrayOf("id"),childColumns = arrayOf("bus_id"),onDelete=CASCADE),
        ForeignKey(entity = Client::class, parentColumns = arrayOf("id"), childColumns = arrayOf("client_id"),onDelete=CASCADE)))
data class Ticket(
    @PrimaryKey @ColumnInfo(name = "id") var id:Int?,
    @ColumnInfo(name = "number") val number:Int?,
    val bus_id:Int?,
    val client_id:Int?):Serializable {
}