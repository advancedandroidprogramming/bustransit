package com.example.bustransit.Data.Remote.Model

class Bus
{
    var id:Int?=null
    val number:Int?=null
    val price:Int?=null
    val route:String?=null
    val startPlace:String?=null
    val destination:String?=null
}