package com.example.bustransit.Data.Local.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.bustransit.Data.Local.Model.Notification

interface NotificationDao {
    @Query("select * from client order by username")
    fun getAllNotification(): LiveData<List<Notification>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addNotification(notification: Notification):Long

    @Query("delete from client")
    fun deleteAllNotification()

    @Query("select * from client where id = :clientId")
    fun findNotificationById(clientId:Int) : LiveData<Notification?>

    @Delete
    fun deleteNotification(notification: Notification):Long

    @Update
    fun updateNotification(notification: Notification):Long
}