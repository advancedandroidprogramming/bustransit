package com.example.bustransit.Data.Local.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "message")
data class Message(
    @PrimaryKey @ColumnInfo(name = "id") var id:Int?,
    @ColumnInfo(name="message") val message:String?
    ) {
}