package com.example.bustransit.Data.Local.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.bustransit.Data.Local.Model.Bus

interface BusDao {
    @Query("select * from bus order by number")
    fun getAllBus(): LiveData<List<Bus>>

    @Query("delete from bus")
    fun deleteAllBus()

    @Query("select * from bus where id = :busId")
    fun findBusById(busId:Int) :LiveData<Bus?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addBus(bus: Bus):Long

    @Delete
    fun deleteBus(bus: Bus):Long

    @Update
    fun updateBus(bus: Bus):Long
}