package com.example.bustransit.Data.Remote.ApiService

import com.example.bustransit.Data.Remote.Model.Bus
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface BusApiService {

    @GET("all")
    fun busList(): Call<List<Bus>>

    @POST("add")
    fun addBus(@Body bus:Bus): Call<Bus>

    @PUT("update/{id}")
    fun updateBus(@Path("id") id:Int?,@Body bus: Bus):Call<Bus>

    @DELETE("delete/{id}")
    fun deleteBus(@Path("id") id:Int):Call<ResponseBody>


    companion object {

        private val baseUrl = "http://10.0.2.2:9090/bus/"

        fun getInstance(): BusApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(BusApiService::class.java)
        }
    }
}