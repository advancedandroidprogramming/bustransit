package com.example.bustransit.Data.Local.Dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.bustransit.Data.Local.Model.Ticket

@Dao
interface TicketDao {
    @Query("select * from ticket order by number")
    fun getAllTicket():LiveData<List<Ticket>>

    @Query("select * from client")
    fun listClient()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun buyTicket(ticket: Ticket):Long

    @Delete
    fun deleteTicket(ticket: Ticket):Long

    @Update
    fun updateTicket(ticket: Ticket):Long

    @Query("select * from ticket where id = :ticketId")
    fun findTicketById(ticketId:Int) :LiveData<Ticket?>

}