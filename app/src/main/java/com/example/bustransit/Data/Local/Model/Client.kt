package com.example.bustransit.Data.Local.Model
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "client")
data class Client(
    @PrimaryKey @ColumnInfo(name = "id") var id:Int?,
    @ColumnInfo(name="username") val username:String?,
    @ColumnInfo(name="password") val password:String?,
    @ColumnInfo(name="phone") val phone:Int?,
    @ColumnInfo(name="relative") val relative:String?
):Serializable {
}