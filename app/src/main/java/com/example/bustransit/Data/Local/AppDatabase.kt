package com.example.bustransit.Data.Local

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Room
import com.example.bustransit.Data.Local.Dao.*
import com.example.bustransit.Data.Local.Model.*

@Database(entities = arrayOf(Bus::class,Client::class,Ticket::class,Notification::class,Feedback::class),version = 1)
abstract class AppDatabase: RoomDatabase() {

    abstract fun busDao():BusDao
    abstract fun clientDao(): ClientDao
    abstract fun ticketDao(): TicketDao
    abstract fun notificationDao():NotificationDao
    abstract fun feedbackDao():FeedbackDao

    companion object {

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {

            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {

                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java, "bus_transit_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }
}