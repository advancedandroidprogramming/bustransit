package com.example.bustransit.Data.Local.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName="bus")
data class Bus(
    @PrimaryKey @ColumnInfo(name="id") var id:Int?,
    @ColumnInfo(name="number") val number:Int?,
    @ColumnInfo(name="price") val price:Int?,
    @ColumnInfo(name="route") val route:String?,
    @ColumnInfo(name="start_place") val startPlace:String?,
    @ColumnInfo(name = "destination") val destination:String?):Serializable {
}