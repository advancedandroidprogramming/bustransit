package com.example.bustransit.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.example.bustransit.R
import com.example.bustransit.data.local.model.Bus
import com.example.bustransit.databinding.FragmentAddBusBinding
import com.example.bustransit.viewmodel.BusViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_add_bus.*
import kotlinx.android.synthetic.main.fragment_bus.*


class AddBusFragment : Fragment() {

    private lateinit var binding:FragmentAddBusBinding
    lateinit var busViewModel:BusViewModel
    private var idSet:Int=0
    lateinit var floatingActionButton: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding=FragmentAddBusBinding.inflate(inflater,container,false)

        busViewModel=ViewModelProviders.of(this).get(BusViewModel::class.java)
        binding.item=busViewModel.bus
        binding.addBus=this
        binding.executePendingBindings()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        /*floatingActionButton=fab_add_bus
        floatingActionButton.setOnClickListener{
            addBus()
        }*/
    }

    fun addBus(view: View)
    {
        //val bus=readFields()
        busViewModel.addBusToRoom(busViewModel.bus)
        Toast.makeText(context,"bus added",Toast.LENGTH_LONG).show()
    }

    /*private fun readFields()=Bus(
        idSet++,
        driver_name_input.text.toString(),
        plate_number_input.text.toString().toInt(),
        bus_type_input.text.toString(),
        travel_time_input.text.toString(),
        price_input.text.toString().toInt(),
        route_input.text.toString(),
        start_place_input.text.toString(),
        destination_input.text.toString()
    )*/


}
