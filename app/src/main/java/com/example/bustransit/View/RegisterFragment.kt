package com.example.bustransit.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

import com.example.bustransit.R
import com.example.bustransit.data.local.model.User
import com.example.bustransit.databinding.FragmentRegisterBinding
import com.example.bustransit.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_add_bus.*
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.bus_type_input
import kotlinx.android.synthetic.main.fragment_register.driver_name_input
import kotlinx.android.synthetic.main.fragment_register.plate_number_input
import kotlinx.android.synthetic.main.fragment_register.route_input

/**
 * A simple [Fragment] subclass.
 */
class RegisterFragment : Fragment() {

    private lateinit var binding:FragmentRegisterBinding
    private lateinit var userModel:UserViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding=FragmentRegisterBinding.inflate(inflater,container,false)

        userModel= ViewModelProviders.of(this).get(UserViewModel::class.java)
        binding.user=userModel.user
        binding.addUser=this
        binding.executePendingBindings()
        return binding.root
    }

    fun addUser(view:View)
    {
        val bus=readFields()

        userModel.addClient(bus)
        Toast.makeText(context,"User added", Toast.LENGTH_LONG).show()
    }

    private fun readFields()= User(
        1,
        driver_name_input.text.toString(),
        plate_number_input.text.toString(),
        bus_type_input.text.toString(),
        route_input.text.toString()
    )


}
