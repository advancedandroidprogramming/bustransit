package com.example.bustransit.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.bustransit.R

/**
 * A simple [Fragment] subclass.
 */
class TicketFragment : Fragment() {

    private lateinit var binding:FragmentTicketBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding=FragmentTicketBinding.inflate(inflater,container,false)
        return binding.root
    }

}
