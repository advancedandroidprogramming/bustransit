package com.example.bustransit.view


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.example.bustransit.data.local.model.Ticket
import com.example.bustransit.databinding.FragmentBusDetailBinding
import com.example.bustransit.viewmodel.TicketViewModel


class BusDetailFragment : Fragment() {

    private lateinit var binding: FragmentBusDetailBinding
    private lateinit var ticketViewModel:TicketViewModel
    private lateinit var ticket:Button
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentBusDetailBinding.inflate(inflater, container, false)
        ticket=binding.buttonBuy
        ticketViewModel=ViewModelProviders.of(this).get(TicketViewModel::class.java)
        val ticketData=Ticket(1,2322,1,1)
        ticket.setOnClickListener {
               ticketViewModel.buyTicket(ticketData)
                Toast.makeText(context,"ticket sold",Toast.LENGTH_LONG).show()
        }
        return binding.root
    }


}
