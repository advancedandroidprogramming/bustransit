package com.example.bustransit.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bustransit.R
import com.example.bustransit.adapter.UserAdapter
import com.example.bustransit.databinding.FragmentProfileBinding
import com.example.bustransit.viewmodel.UserViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

import androidx.navigation.fragment.findNavController
import com.example.bustransit.data.local.model.Bus
import com.example.bustransit.data.local.model.User
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var userModel:UserViewModel
    lateinit var floatingActionButton: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)

        binding.recyclerListNotification.layoutManager=LinearLayoutManager(requireContext())

        userModel=ViewModelProviders.of(this).get(UserViewModel::class.java)

        val adapter=UserAdapter()
        binding.recyclerListNotification.adapter=adapter

        userModel.allClient.observe(this, Observer {
            users -> users?.let {
            adapter.setUser(users)
        }
        })

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        floatingActionButton=fab_add_user
        floatingActionButton.setOnClickListener {
            it.findNavController().navigate(R.id.user_add_nav)
        }
    }

    /*fun navigateToDetail(view: View) {
        findNavController().navigate(R.id.user_add_nav)
    }*/
}
