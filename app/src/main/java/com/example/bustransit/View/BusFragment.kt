package com.example.bustransit.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bustransit.R
import com.example.bustransit.adapter.BusAdapter
import com.example.bustransit.databinding.FragmentBusBinding
import com.example.bustransit.viewmodel.BusViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_bus.*


class BusFragment : Fragment() {

    lateinit var busView:BusViewModel
    lateinit var floatingActionButton:FloatingActionButton
    private var columnCount = 1
    private lateinit var binding:FragmentBusBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       /* val view=inflater.inflate(R.layout.fragment_bus,container,false)

        val app=activity?.application
        val busAdapter:BusAdapter
        busView=ViewModelProviders.of(this).get(BusViewModel::class.java)
        if(view is RecyclerView)
        {
            with(view)
            {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }
                addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
                busAdapter = BusAdapter(busView)
                adapter = busAdapter
            }
                val b=busView.findViewByNumber(1).value?.route.toString()
                Toast.makeText(this.context,b,Toast.LENGTH_LONG).show()
                busView.getAllBuses().observe(this, Observer {
                    buses -> busAdapter.setBuses(buses)
                })

        }
        return view*/

        binding=FragmentBusBinding.inflate(inflater,container,false)

        binding.recyclerListBus.layoutManager=LinearLayoutManager(requireContext())

        busView=ViewModelProviders.of(this).get(BusViewModel::class.java)

        //binding.busViewModel=busView

        val adapter= BusAdapter()

        binding.recyclerListBus.adapter=adapter
        busView.allBus.observe(this, Observer {
                buses -> buses?.let { adapter.setBuses(buses) }
        })

        return binding.root

    }

    override fun onStart() {
        super.onStart()
        /*floatingActionButton=add_bus_fab
        floatingActionButton.setOnClickListener {
            it.findNavController().navigate(R.id.bus_add_nav)
        }*/
    }

    /*fun navigateToDetail(view: View) {
        findNavController().navigate(R.id.bus_to_detail)
    }*/

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}
