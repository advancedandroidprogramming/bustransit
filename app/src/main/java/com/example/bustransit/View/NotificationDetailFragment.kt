package com.example.bustransit.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.bustransit.R
import com.example.bustransit.databinding.FragmentNotificationDetailBinding

/**
 * A simple [Fragment] subclass.
 */
class NotificationDetailFragment : Fragment() {

    private lateinit var binding:FragmentNotificationDetailBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding=FragmentNotificationDetailBinding.inflate(inflater,container,false)
        return binding.root
    }


}
