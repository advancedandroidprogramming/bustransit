package com.example.bustransit.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.bustransit.Data.Local.Model.Ticket
import com.example.bustransit.Repository.TicketRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TicketViewModel(app:Application):AndroidViewModel(app) {
    private val ticketRepository= TicketRepository(app)

    init {
        ticketRepository.asyncTicketData()
    }

    val allTicket:LiveData<List<Ticket>>
        get() = ticketRepository.ticketList

    fun buyTicket(ticket:Ticket)= viewModelScope.launch(Dispatchers.IO)
    {
        ticketRepository.buyTicket(ticket)
    }

    fun deleteTicket(ticket:Ticket)= viewModelScope.launch(Dispatchers.IO)
    {
        ticketRepository.deleteTicket(ticket)
    }

    fun updateTicket(ticket:Ticket)= viewModelScope.launch(Dispatchers.IO)
    {
        ticketRepository.updateTicket(ticket)
    }

    fun findTicketById(id:Int):LiveData<Ticket?>
    {
        return ticketRepository.findTicketById(id)
    }
}