package com.example.bustransit.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.bustransit.Data.Local.Model.Client
import com.example.bustransit.Repository.ClientRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ClientViewModel(app:Application):AndroidViewModel(app) {

    private val clientRepository = ClientRepository(app)

    init {
        clientRepository.asyncClientData()
    }

    val allClient : LiveData<List<Client>>
        get()=clientRepository.allClient

    fun addClient(client:Client)= viewModelScope.launch(Dispatchers.IO)
    {
        clientRepository.addClient(client)
    }

    fun updateClient(client: Client)= viewModelScope.launch(Dispatchers.IO)
    {
        clientRepository.updateClient(client)
    }

    fun deleteClient(client: Client)= viewModelScope.launch(Dispatchers.IO)
    {
        clientRepository.deleteClient(client)
    }

    fun findClientById(id:Int):LiveData<Client?>
    {
        return clientRepository.findClientById(id)
    }


}