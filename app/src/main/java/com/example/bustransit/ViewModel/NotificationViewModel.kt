package com.example.bustransit.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.bustransit.Data.Local.Model.Notification
import com.example.bustransit.Repository.NotificationRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotificationViewModel(app: Application): AndroidViewModel(app) {
    private val busRepository = NotificationRepository(app)

    init {
        busRepository.asyncNotificationData()
    }

    val allBus: LiveData<List<Notification>>
        get() = busRepository.ticketList

    fun addBus(bus: Notification) = viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.sendNotification(bus)
    }

    fun deleteBus(bus: Notification) = viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.deleteTicket(bus)
    }

    fun updateBus(bus: Notification) = viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.updateTicket(bus)
    }

    fun findViewById(id: Int): LiveData<Notification?> {
        return busRepository.findTicketById(id)
    }
}