package com.example.bustransit.ViewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.bustransit.Data.Local.AppDatabase
import com.example.bustransit.Data.Local.Model.Bus
import com.example.bustransit.Repository.BusRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BusViewModel(app:Application): AndroidViewModel(app) {

    private val busRepository = BusRepository(app)

    init
    {
        busRepository.asyncBusData()
    }

    val allBus : LiveData<List<Bus>>
        get() =busRepository.allBus

    fun addBus(bus: Bus) = viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.addBusToApi(bus)
    }

    fun deleteBus(bus:Bus) = viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.deleteBusFromRemote(bus)
    }

    fun updateBus(bus: Bus) = viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.updateBusRoom(bus)
    }

    fun deleteAllBus()= viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.deleteAllBusRoom()
    }

    fun addBusToRoom(bus: Bus)= viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.addBusToRoom(bus)
    }

    fun deleteBusFromRoom(bus: Bus)= viewModelScope.launch(Dispatchers.IO)
    {
        busRepository.deleteBusRoom(bus)
    }

    fun findViewById(id:Int):LiveData<Bus?>
    {
        return busRepository.findBusById(id)
    }
}